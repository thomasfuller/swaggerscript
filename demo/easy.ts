import { API, Resource, Parameter, PrimitiveSchema, ObjectSchema, ArraySchema, Response, Header, Content } from "../src/S";


let api = new API("1.0.0", "sample app", "UW");
let endpoint = new Resource("endpoint");
endpoint.addMethod("post")
    .addResponse(new Response("default")
        .setDescription("default"));
api.addResource(endpoint);

console.log(api.output());