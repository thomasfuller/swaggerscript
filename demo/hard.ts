import { API, Resource, Parameter, PrimitiveSchema, ObjectSchema, ArraySchema, Response, Header, Content } from "../src/S";

/**
 * The SwaggerScript equivalent of petstore.json
 * 
 * To run:
 * tsc ES6
 * node petstore.js
 * 
 */

let api = new API("1.0.0", "Hard Petstore", "MIT")
.addServer("http://petstore.swagger.io/v1");

let pet = new ObjectSchema()
.addProperty("id", new PrimitiveSchema("integer", "int64"), true)
.addProperty("name", new PrimitiveSchema("string"), true)
.addProperty("tag", new PrimitiveSchema("string"), false);

let petsContent = new Content("application/json", new ArraySchema(pet));

let error = new ObjectSchema()
.addProperty("code", new PrimitiveSchema("integer", "int32"), true)
.addProperty("message", new PrimitiveSchema("string"), true);

let errorResponse = new Response("default")
    .setDescription("unexpected error")
    .addContent(new Content("application/json", error));

api.addDefaultResponse(errorResponse);


let petsPath = new Resource("pets");
petsPath.addMethod("get")
    .addTags("pets")
    .setOperationId("listPets")
    .addParameters(new Parameter("limit", "query", false)
        .setDescription("How many items to return at one time (max 100)")
        .setSchema(new PrimitiveSchema("integer", "int32")))
    .addResponse(new Response(200)
        .setDescription("A paged array of pets")
        .addHeader(new Header("x-next", "A link to the next page of responses", new PrimitiveSchema("string")))
        .addContent(petsContent))

petsPath.addMethod("post")
    .addTags("pets")
    .setOperationId("createPets")
    .addResponse(new Response(201)
        .setDescription("Null response"))

let storePath = new Resource("store");

storePath.addMethod("get")
    .setSummary("Gets the store inventory")
    .setOperationId("getInventory")
    .addParameters(new Parameter("number", "query", false)
        .setDescription("How many items to return")
        .setSchema(new PrimitiveSchema("integer", "int32")))
    .addTags("store")
    .addResponse(new Response(200)
        .setDescription("expected response to a valid request")
        .addContent(petsContent))

let userPath = new Resource("user");

userPath.addMethod("post")
    .setSummary("Log in")
    .setOperationId("loginUser")
    .addTags("login")
    .addParameters(new Parameter("username", "header", true)
            .setDescription("Username")
            .setSchema(new PrimitiveSchema("string")),
        new Parameter("password", "header", true)
            .setDescription("User password")
            .setSchema(new PrimitiveSchema("string")))
    .addResponse(new Response(200)
        .setDescription("Successful login"));

// slightly change the login
let loginError = new Response("default")
    .setDescription("Failed login")
    .addContent(new Content("application/json", error));

// Change the default response for the user path
userPath.addDefaultResponse(loginError);

api.addResource(petsPath, "/pets")
    .addResource(storePath, "/store")
    .addResource(userPath, "/user");

let output = api.output();

console.log(output);