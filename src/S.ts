

// TODO: Apparently there are some characters like & that are sort of allowed. This should be considered.
const PATH_REGEX = new RegExp("^([a-zA-Z0-9]*)$|(^{([a-zA-Z0-9]*)})$");
const NUMBER_REGEX = new RegExp("^[0-9]$");

const ALLOWED_METHODS = ["get", "post"];


export class APINode {
    defaults: {[val: string]: APINode[]} = {};

    _getAsJson(parentDefaults: {[val: string]: APINode[]}): {} {
        throw new Error("Not implemented yet");
    }

    clone(): APINode {
        throw new Error("Not implemented yet");
    }

    _addDefaultResponse(res: Response) {
        if (this.defaults[`defaultResponse`] == undefined) {
            this.defaults[`defaultResponse`] = [];
        }
        this.defaults[`defaultResponse`].push(res);
    }

    /**
     * Takes the default values from the parent and inserts them into this node's
     * defaults, but does not replace existing ones
     */
    _mergeInDefaults(parentDefaults: {[val: string]: APINode[]}) {
        for (let key in parentDefaults) {
            if (this.defaults[key] == undefined) {
                this.defaults[key] = parentDefaults[key];
            } else if (parentDefaults[key] == undefined){
                // do nothing, defaults already set
            } else {
                /**
                 * IMPORTANT: Merge strategy is just to put the child defaults before the 
                 * parent defaults. This works for Response, for example, because response
                 * will check if the response type has already been defined for the method.
                 * So, it will add the new one first, then see the old one later and skip it.
                 * 
                 * This is very important because it may or may not be compatible with future
                 * defaults, if not, then will have to rethink.
                 */
                this.defaults[key] = this.defaults[key].concat(parentDefaults[key]);
            }
        }
    }
}

export class API extends APINode {
    info: {};
    servers = [];
    rootPaths: Resource[] = [];

    /**
     * 
     * @param version: the version of the API. Example: "1.0.0"
     * @param title: the title of the API.
     * @param licenseName: license name. Example: "MIT"
     */
    constructor(version: string, title: string, licenseName: string) {
        super();
        this.info = {
            "version": version,
            "title": title,
            "license": {
                "name": "MIT"
            }
        }
    }

    /**
     * Adds Resource resource to the API.
     * 
     * @param resource: The resource to be added
     * @param name?: If specified, this is the location resource should be added to. Otherwisem, root.
     *      name should be in path format /like/this
    */
    addResource(resource: Resource, name?: String) : API {
        // Simply adds the resource as a top level resource to the API
        // if no name specified, or if the name specifies the root
        if (name == undefined || name.split("/").length == 2) {
            if (this.rootPaths.find((el) => el.getName() == resource.getName()) != undefined) {
                throw new Error(`Resource at ${resource.getName()} has already been defined`);
            }
            this.rootPaths.push(resource);
            return this;
        }

        // should start with /, so skip the first empty string
        let paths = name.split("/").slice(1);

        // first resource will be a root level resource, so have to pick it out first
        let node = this.rootPaths.find((el) => el.getName() == paths[0]);
        if (node == undefined) {
            node = new Resource(paths[0]);
            this.rootPaths.push(node);
        }

        // Go down the path. 3 possible cases:
        //      1) it's the end of paths, in which case resource is added
        //      2) the next name exists, in which case replace node to continue
        //      3) the next name doesn't exist, in which case it creates it
        for (let i = 1; i < paths.length; i++) {
            let childMatch = node._getChild(paths[i]);
            if (i == paths.length - 1) {
                if (childMatch != undefined) {
                    throw new Error(`Resource at ${name} has already been defined`);
                }
                node.addResource(resource);
            } else if (childMatch != undefined) {
                node = childMatch;
            } else {
                let next = new Resource(paths[i]);
                node.addResource(next);
                node = next;
            }
        }

        return this;
    }

    /**
     * All methods in the defined paths will automatically have this default 
     * response set
     * 
     * @param res the default response to be added
     */
    addDefaultResponse(res: Response) : API {
        this._addDefaultResponse(res);
        return this;
    }

    setInfo(info: {}): API {
        this.info = info;
        return this;
    }

    addServer(url: string, desc?: string): API {
        let toAdd = {
            "url": url
        }
        if (desc != undefined) {
            toAdd["description"] = desc
        }
        this.servers.push(toAdd);
        return this;
    }
    

    /**
     * Returns the OpenAPI JSON string representation of this API.
     */
    output(): string {
        let paths = {};
        for (let i = 0; i < this.rootPaths.length; i++) {
            paths = Object.assign({}, paths, this.rootPaths[i]._getAsJson(this.defaults));
        }
        let res = {
            "openapi": "3.0.0", // At some point allow for different versions?
            "paths": paths,
            "servers": this.servers,
            "info": this.info
        };

        return JSON.stringify(res, null, 4);
    }
}

// TODO: We need to allow path fields like /pet/{name}
export class Resource extends APINode {
    parent: Resource = null;
    name: string = null;
    children: Resource[] = [];
    methods: {[method: string]: Method | undefined} = {};

    /**
     * Creates a new, blank ResourceNode.
     * 
     * @param name of the resource. The name must not include "/" and must be compliant
     *      with URL rules as defined in RFC 1738: http://www.faqs.org/rfcs/rfc1738.html
     * 
     * @throws An error if the provided name is not a valid path.
     */
    constructor(name: string) {
        super();

        name = name.trim();
        if (name.indexOf("/") != -1) {
            throw new Error("You are not allowed to include / in the resource name. Create a subresource instead.");
        }
        if (!PATH_REGEX.test(name)) {
            throw new Error(`The provided name ${name} is invalid. Must match ${PATH_REGEX}.`);
        }
        this.name = name;
    }

    getParent(): Resource {
        return this.parent;
    }

    /**
     * This resource and all child resources will automatically have
     * this default response added to all methods
     * 
     * @param res the default response to be added
     */
    addDefaultResponse(res: Response) : Resource {
        this._addDefaultResponse(res);
        return this;
    }

    /**
     * Adds a child resource to this resource and returns the newly created child.
     * 
     * Passing in a string creates a new ResourceNode as a child and returns the child.
     * For example, if this resource is "/example", calling
     * addResource("test") creates and returns a resource node with
     * the path "/example/test".
     * 
     * Alternatively, passing a ResourceNode adds a copy of that as a child.
     * the ResourceNode's path is concatenated onto the path of this node.
     * For example, if this resource is "/example", and the passed in node
     * has a name "test", the child's path becomes "/example/test". 
     * 
     * @param name 
     */
    addResource(resource: Resource): Resource {
        if (resource == null) {
            throw new Error(`resource is undefined at path ${this.getPath()}/${resource.getName()}`)
        }
        this.children.push(resource);
        resource.setParent(this);
        return resource;
    }
    
    /**
     * Adds a new method to the resource.
     * If that method already exists, it is overwritten.
     * If an existing method is added, copies that method so that any future changes to the method
     * are not reflected in this resources method.
     * 
     * @param method Name of the method to add, or if adding an existing Method, that method.
     * @returns the method added to the resource, or null if unable to add the method.
     * @throws An error if an unsupported method is attempted to be added.
     */
    addMethod(method: string | Method): Method {
        if (typeof method === "string") {
            method = cleanString(method);
            let methodNode = new Method(method);
            this.methods[method] = methodNode
            return methodNode;
        } else if (method instanceof Method) {
            let cloned = method.clone();
            this.methods[cloned.getMethodName()] = cloned;
            return cloned;
        }
    }

    /**
     * Sets the provided ResourceNode as the parent to this node.
     * 
     * @param parent to make this the child of.
     */
    setParent(parent: Resource) {
        this.parent = parent;
    }

    /**
     * Given a method name, returns the associated method.
     * 
     * @param methodName to get the method for
     * @returns the method, or null if it does not exist.
     */
    getMethod(methodName: string): Method | null {
        methodName = cleanString(methodName);
        if (methodName in this.methods) {
            return this.methods[methodName];
        }
        return null;

    }

    /**
     * @returns the methods attached to this resource
     */
    getMethods(): {[method: string]: Method | undefined} {
        // Clone the result so we don't expose internal structures.
        // See https://stackoverflow.com/questions/5055746/cloning-an-object-in-node-js
        return JSON.parse(JSON.stringify(this.methods));
    }

    /**
     * @returns the children resources of this resource.
     */
    getChildren(): Resource[] {
       // Clone the result so we don't expose internal structures.
       return this.children.slice();
    }

    /**
     * @param name: name of the resource to be fetched.
     * @returns the child resource with name name.
     */
    _getChild(name : String) : Resource {
        return this.children.find((el) => el.getName() == name);
    }

    /**
     * Returns the name of this resource.
     */
    getName(): string {
        return this.name;
    }

    /**
     * Returns the full path of this resource.
     */
    getPath(): string {
        let parentPath = "/";
        if (this.parent != null) {
            parentPath = this.parent.getPath();
        }
        if (parentPath.endsWith("/")) {
            return parentPath + this.name;
        } else {
            return parentPath + "/" + this.name;
        }
    }

    /**
     * Creates a JSON OpenAPI compliant string of this resource.
     */
    _getAsJson(parentDefaults: {[val: string]: APINode[]}): {} {
        this._mergeInDefaults(parentDefaults);
        
        let path = this.getPath();
        let methods = this.getMethods();
        let children = this.getChildren();

        let thisLevel = {};
        let result = {}

        for (let methodName in methods) {
            thisLevel[methodName] = this.methods[methodName]._getAsJson(this.defaults);
        }
        
        // don't add things without methods
        if (Object.keys(thisLevel).length > 0) {
            result[path] = thisLevel;
        }

        for (let resource in children) {
            result = Object.assign({}, result, children[resource]._getAsJson(this.defaults));
        }

        return result;
    }
}

export class Header extends APINode {
    name: string
    description : string
    content: Schema

    constructor(name: string, description: string, content: Schema) {
        super();
        this.name = name;
        this.description = description;
        this.content = content;
    }

    _getAsJson(parentDefaults: {[val: string]: APINode[]}): {} {
        this._mergeInDefaults(parentDefaults);

        let res = {};
        let contents = {
            "description": this.description,
            "schema": this.content._getAsJson(this.defaults),
        }
        res[this.name] = contents;

        return res;
    }

}

export class Parameter extends APINode {
    name: string
    inLoc: string // "query" "header", "path", or "cookie"
    description: string
    required: boolean
    schema: Schema

    constructor(name: string, inLoc: string, required: boolean) {
        super()
        this.name = name;
        this.inLoc = inLoc;
        this.required = required;
    }

    setDescription(desc: string) : Parameter {
        this.description = desc;
        return this;
    }

    setSchema(schema: Schema) : Parameter {
        this.schema = schema;
        return this;
    }

    _getAsJson(defaults: {[val: string]: APINode[]}): {} {
        let result = {
            "name": this.name,
            "in": this.inLoc,
            "required": this.required
        }

        if (this.description != undefined) {
            result["description"] = this.description;
        }

        if (this.schema != undefined) {
            result["schema"] = this.schema._getAsJson(this.defaults);
        }

        return result;
    }
}

export class Method extends APINode {
    method: string;
    responses: Response[] = [];
    tags: string[] = [];
    summary: string;
    parameters: Parameter[] = [];
    operationId;

    constructor(method: string) {
        super();
        if (ALLOWED_METHODS.indexOf(cleanString(method)) == -1) {
            throw new Error("Unsupported method. Supported methods are get and post");
        }
        this.method = method;
    }

    // Responses

    // The "Response" class handles internal logic of responses

    /**
     * Sets the response types to the provided response types.
     * This is distinct from addResponseType() in that it replaces the
     * previous response types whereas addResponseType() simply adds them.
     * 
     * @param args List of response types to set to this method.
     * @returns This MethodNode, to allow for call chaining.
     */
    setResponse(...args: Response[]): Method {
        this.clearResponses();
        this._addResponses(args);
        return this;
    }

    /**
     * Adds the response types to the provided response types.
     * This is distinct from setResponseTypes() in that it does not remove 
     * any existing response types. If a response type is duplicated, then
     * it is not added a second time.
     * 
     * @param args List of response types to add to this method.
     * @returns This MethodNode, to allow for call chaining.
     */
    addResponse(...args: Response[]): Method {
        this._addResponses(args);
        return this;
    }

    /**
     * Removes all responses from this Method.
     * 
     * @returns This MethodNode, to allow for call chaining.
     */
    clearResponses(): Method {
        this.responses = [];
        return this;
    }

    /**
     * Adds the list of responses as responses for this method.
     * 
     * @param args of Responses to add
     */
    _addResponses(args: Response[]) {
        args.forEach(r => {
            this.responses.push(r);
        });
    }

    // Tags

    setTags(...tags: string[]): Method {
        this.removeTags();
        this._addTags(tags);
        return this;
    }

    addTags(...tags: string[]): Method {
        this._addTags(tags);
        return this;
    }

    removeTags(): Method {
        this.tags = [];
        return this;
    }

    _addTags(tags: string[]) {
        tags.forEach(r => {
            this.tags.push(r);
        });
    }

    // Parameters

    // Parameters are handled by the "Parameter" class
    // TODO: runtime check on parameters based on parent to 
    //      see if parameters are valid.

    setParameters(...params: Parameter[]): Method {
        this.removeParameters()
        this._addParams(params);
        return this;
    }

    addParameters(...params: Parameter[]): Method {
        this._addParams(params);
        return this;
    }

    removeParameters(): Method {
        this.parameters = [];
        return this;
    }

    _addParams(params: Parameter[]) {
        params.forEach(r =>  {           
            this.parameters.push(r);
        });
    }

    // Summary

    setSummary(summary: string): Method {
        this.summary = summary;
        return this;
    }

    // OperationId

    setOperationId(id: string): Method {
        this.operationId = id;
        return this;
    }

    // Other functions

    getMethodName(): string {
        return this.method;
    }

    clone(): Method {
        let result = new Method(this.method);
        Object.assign(result, this);
        return result;
    }

    _getAsJson(parentDefaults: {[val: string]: APINode[]}) {
        this._mergeInDefaults(parentDefaults);

        if(this.defaults["defaultResponse"] != undefined) {
            // This could be cut to linear time by using a map to track response names, but probs unnecessary
            this.defaults["defaultResponse"].forEach((el) => {
                if (el instanceof Response) {
                    // only add it in if there isn't already a response of this type
                    if (this.responses.find((val) => val.getName() == el.getName()) == undefined) {
                        this.addResponse(el);
                    }
                } else {
                    throw new Error("A non-Response type APINode made it into the response list")
                }
            })
        }

        let res = {};

        if (this.tags.length > 0) {
            res["tags"] = this.tags;
        }

        if (this.operationId != undefined) {
            res["operationId"] = this.operationId;
        }
        
        if (this.summary != undefined) {
            res["summary"] = this.summary;
        }

        if (this.responses.length > 0) {
            res["responses"] = {}
            this.responses.forEach(e => {
                res["responses"][e.getName()] = e._getAsJson(this.defaults);
            });
        }

        if (this.parameters.length > 0) {
            res["parameters"] = [];
            this.parameters.forEach(e => {
                res["parameters"].push(e._getAsJson(this.defaults));
            });
        }

        return res;
    }
}

export class Response extends APINode {
    name: string;
    description: string;
    content: Content[] = [];
    headers: Header[] = [];

    constructor(name?: string | number) {
        super()
        if (name == undefined) {
            this.name = "default";
            return;
        } 
        if (typeof name == "string") {
            name = cleanString(name);

            // Default is the only non-numeric value allowed. We must store as a string regardless.
            if (name == "default") {
                this.name = "default";
            } else if (NUMBER_REGEX.test(name)) {
                this.name = name;
            } else {
                throw new Error("Response name must be 'default' or numeric.")
            }

        } else if (typeof name =="number") {
            this.name = name.toString(10);
        }
    }

    setDescription(desc: string): Response {
        this.description = desc;
        return this;
    }

    addHeaders(...headers: Header[]): Response {
        headers.forEach((el) => {
            this.addHeader(el);
        })
        return this;
    }

    removeHeaders(): Response {
        this.headers = [];
        return this;
    }

    addHeader(header: Header): Response {
        this.headers.push(header);
        return this;
    }

    getName(): string {
        return this.name;
    }

    addContents(content: Content[]) : Response {
        content.forEach((el) => this.addContent(el));
        return this;
    }

    addContent(content: Content): Response {
        this.content.push(content);
        return this;
    }

    _getAsJson(defaults: {[val: string]: APINode[]}): {} {
        let res = {
            "description": this.description
        };
        if (this.content.length > 0) {
            let content = {}
            // combine all of the content into a single level dict
            this.content.forEach((el) => {
                content = Object.assign({}, content, el._getAsJson(this.defaults));
            });
            res["content"] = content;
        }
        return res;
    }
}

/**
 * See https://swagger.io/docs/specification/describing-responses/
 */
export class Content extends APINode {
    type: string
    schema: Schema

    constructor(type: string, schema?: Schema) {
        super();
        this.type = type;
        if (schema != undefined) {
            this.schema = schema
        }
    }

    getType(): string {
        return this.type;
    }
    
    setType(type: string): Content {
        this.type = type;
        return this;
    }

    getSchema(): Schema {
        return this.schema;
    }

    setSchema(schema: Schema): Content {
        this.schema = schema;
        return this;
    }

    _getAsJson(defaults: {[val: string]: APINode[]}): {} {
        if (this.schema == undefined) {
            throw new Error(`A Content schema of type ${this.type} was left null`);
        }

        // JS threw a fit when I tried to have this.type as the key
        let schema = { "schema": this.schema._getAsJson(this.defaults) };
        let result = {};
        result[this.type] = schema;
        return result;
    }

}

export class Schema extends APINode {
    /**
     * Important to avoid circularly defined schemas, currently will NOT detect these
     */

     _getAsJson(parentDefaults: {[val: string]: APINode[]}): {} {
        throw new Error("Not implemented yet");
     }
}

export class ArraySchema extends Schema {
    itemDef: Schema

    constructor(itemDef : Schema) {
        super()
        this.itemDef = itemDef;
    }

    _getAsJson(parentDefaults: {[val: string]: APINode[]}): {} {
        this._mergeInDefaults(parentDefaults);

        return {
            "type": "array",
            "items": this.itemDef._getAsJson(this.defaults)
        };
    }
}

export class ObjectSchema extends Schema {
    properties: {[s: string]: Schema}  = {} // name:content
    requiredProperties: string[] = []
    additionalProperties: boolean;
    minAdditional: number
    maxAdditional: number

    constructor() {
        super()
    }


    /**
      * Only valid for type Object, all other types will error.
      * 
      * Adds a new property to the Schema.
      * 
      * @param name: Name of property to be added
      * @param value: Value of property to be added
      * @param isRequired: Whether this is a required property in the object
      */
     addProperty(name: string, value: Schema, isRequired: boolean) : ObjectSchema {
        this.properties[name] = value;
        if (isRequired) {
            this.requiredProperties.push(name);
        }
        return this;
     }


     /**
      * Only valid for type Object, all other types will error. 
      * 
      * Defining Schema as type object with no properties is equivalent to additionalProperties = true
      * 
      * @param additional : boolean: Whether additional properties are allowed
      * @param min : Minimum number of additional properties
      * @param max : Maximum number of additional properties
      */
     allowsAdditionalProperties(additional: boolean, min?: number, max?: number) : ObjectSchema {
        this.additionalProperties = additional;
        this.minAdditional = min;
        this.maxAdditional = max;

        return this;
     }

     _getAsJson(parentDefaults: {[val: string]: APINode[]}): {} {
        this._mergeInDefaults(parentDefaults);

        let result = {
            "type": "object"
        }
        
        if (this.allowsAdditionalProperties.length > 0) {
            result["additionalProperties"] = this.allowsAdditionalProperties;
            if (this.minAdditional != undefined) {
                result["minProperties"] = this.minAdditional;
            }
            if (this.maxAdditional != undefined) {
                result["maxProperties"] = this.maxAdditional;
            }
        }

        let properties = {};
        for (let key in this.properties) {
            properties[key] = this.properties[key]._getAsJson(this.defaults);
        }

        result["properties"] = properties;
        result["required"] = this.requiredProperties.slice();

        return result;
     }
}

export class PrimitiveSchema extends Schema {
    type: string
    format: string
    nullable: boolean;

    constructor(type: string, format?: string) {
        super();
        if (type == "string") {
            // format can be anything
            this.type = type
            this.format = format;
            return;
        } else if (type == "integer") {
            this.type = type;
            // format can be blank, int32, int64
            if (format == undefined) {
                return;
            }
            if (format != "int32" && format != "int64") {
                throw new Error("integer type can only be blank, int32, or int64");
            }
            this.format = format;
            return;
        } else if (type == "number") {
            // format can be blank, float, double
            if (format == undefined) {
                this.type = type;
                return;
            }
            if (format != "float" && format != "double") {
                throw new Error("number type can only be blank, float, or double");
            }
            return;
        } else if (type == "boolean") {
            this.type = type;
            return;
        }
        throw new Error(`Type ${type} is not a valid type.`)
    }

    /**
     * Nullable is false be default
     * 
     * @param nullable whether or not this primitive is nullable
     */
    setNullable(nullable: boolean): PrimitiveSchema {
        this.nullable = nullable;
        return this;
    }

    _getAsJson(parentDefaults: {[val: string]: APINode[]}): {} {
        this._mergeInDefaults(parentDefaults);

        let result = {
            "type": this.type
        };

        if (this.format != undefined) {
            result["format"] = this.format;
        }

        if(this.nullable != undefined) {
            result["nullable"] = this.nullable;
        }

        return result;
    }
}


// Utility functions

function cleanString(s: string): string {
    s = s.trim().toLowerCase();
    return s;
}