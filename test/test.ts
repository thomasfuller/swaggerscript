import assert = require('assert');
import chai = require('chai');
import rewire = require('rewire');
import sinon = require('sinon');
import { API, Resource, Method, Response, Schema, ArraySchema, PrimitiveSchema, ObjectSchema, Content, Header } from "../src/S";

const should = chai.should();
const expect = chai.expect;

describe("Basics", function() {
    beforeEach(function (done) {
        // any setup needed goes here
        // Don't remove `done()`
        done();
    });

    it("Can add a single resource that has a GET method", function() {
        let pet = new Resource("pet");
        pet.addMethod("get").addResponse(
            new Response().setDescription("unexpected error")
        );

        let api = new API("1.0.0", "Swagger Petstore", "MIT");
        let root = api.addResource(pet);

        let out = api.output();
    });


});

describe("Response", function() {
    it("Throws an error for non-numeric responses", function() {
        expect(() => new Response("four-zero-four file not found")).to.throw();
    });
    it("Returns a default response if no name is provided", function() {

    });
});

describe("API", function() {
    // Put API class tests here
});

describe("Resource", function() {
    let basicGet;
    let basicGetContents;
    
    beforeEach(function (done) {
        // any setup needed goes here
        // Don't remove `done()`
        basicGet = new Method("get").addResponse(
            new Response().setDescription("unexpected error")
        );

        basicGetContents = {
            "responses": {
                "default": {
                    "description": "unexpected error"
                }
            }
        }

        done();
    });
    
    it("should handle nested resources", function() {

        let actual = {
            "openapi": "3.0.0",
            "info": {
                "version": "1.0.0",
                "title": "Swagger Petstore",
                "license": {
                    "name": "MIT"
                }
            },
            "servers": [{"url":"http://petstore.swagger.io/v1"}],
            "paths": {
                "/pet": {
                    "get": basicGetContents
                },
                "/pet/findByTags": {
                    "get": basicGetContents
                }
            }
        }

        let pet = new Resource("pet");
        pet.addMethod(basicGet);

        let findByTags = new Resource("findByTags");
        findByTags.addMethod(basicGet);
        pet.addResource(findByTags);

        let result = new API("1.0.0", "Swagger Petstore", "MIT")
        .addServer("http://petstore.swagger.io/v1")
        .addResource(pet)
        .output();

        expect(JSON.parse(result)).to.be.deep.equal(actual);
    });

    it("should not render methodless resources", function(){
        let actual = {
            "openapi": "3.0.0",
            "info": {
                "version": "1.0.0",
                "title": "Swagger Petstore",
                "license": {
                    "name": "MIT"
                }
            },
            "servers": [],
            "paths": {
                "/pet/findByTags": {
                    "get": basicGetContents
                }
            }
        }

        // Create /pet/findByTags, but leave /pet methodless
        let findByTags = new Resource("findByTags");
        findByTags.addMethod(basicGet);
        let pet = new Resource("pet");
        pet.addResource(findByTags);

        let result = new API("1.0.0", "Swagger Petstore", "MIT")
        .addResource(pet)
        .output();

        expect(JSON.parse(result)).to.be.deep.equal(actual);
    });

    it("should allow nested resources to be added to the top level API", function() {
        let actual = {
            "openapi": "3.0.0",
            "info": {
                "version": "1.0.0",
                "title": "Swagger Petstore",
                "license": {
                    "name": "MIT"
                }
            },
            "servers": [],
            "paths": {
                "/pet": {
                    "get": basicGetContents
                },
                "/pet/findByTags": {
                    "get": basicGetContents
                }
            }
        }

        let findByTags = new Resource("findByTags");
        findByTags.addMethod(basicGet);
        
        let pet = new Resource("pet");
        pet.addMethod(basicGet);

        let result = new API("1.0.0", "Swagger Petstore", "MIT")
        .addResource(pet, "/pet")
        .addResource(findByTags, "/pet/findByTags")
        .output();

        expect(JSON.parse(result)).to.be.deep.equal(actual);
    });
});

describe("Root", function() {
    // Put Root class tests here
});

describe("Method", function() {
    // Put Method class tests here
});

describe("Response", function() {
    // Put Response class tests here
});

describe("Header", function() {
    // Put Header class tests here
    let actual = {
        "x-next": {
          "description": "A link to the next page of responses",
          "schema": {
            "type": "string"
          }
        }
    }

    let header = new Header("x-next", "A link to the next page of responses", new PrimitiveSchema("string"));

    expect(header._getAsJson({})).to.be.deep.equal(actual);
});

describe("Parameter", function() {
    // Put Parameter class tests here
});

describe("Schema", function() {
    it("should create multi parameter object schemas", function() {
        let actual = {
            "openapi": "3.0.0",
            "info": {
                "version": "1.0.0",
                "title": "Swagger Petstore",
                "license": {
                    "name": "MIT"
                }
            },
            "servers": [],
            "paths": {
                "/pet/findByTags": {
                    "get": {
                        "responses": {
                            "default": {
                                "description": "unexpected error",
                                "content" : {
                                    "application/json": {
                                        "schema": {
                                            "type": "object",
                                            "required": [
                                                "code",
                                                "message"
                                            ],
                                            "properties": {
                                                "code": {
                                                    "type": "integer",
                                                    "format": "int32"
                                                },
                                                "message": {
                                                    "type": "string"
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        let obj = new ObjectSchema()
        .addProperty("code", new PrimitiveSchema("integer", "int32"), true)
        .addProperty("message", new PrimitiveSchema("string"), true);

        let findByTags = new Resource("findByTags");
        findByTags.addMethod("get")
        .setResponse(new Response("default")
            .addContent(new Content("application/json", obj))
            .setDescription("unexpected error")
        );

        let api = new API("1.0.0", "Swagger Petstore", "MIT")
        .addResource(findByTags, "/pet/findByTags")
        .output();

        expect(JSON.parse(api)).to.be.deep.equal(actual);
    });

    it("Should allow schema reuse", function() {
        let actual = {
            "openapi": "3.0.0",
            "info": {
                "version": "1.0.0",
                "title": "Swagger Petstore",
                "license": {
                    "name": "MIT"
                }
            },
            "servers": [],
            "paths": {
                "/pet/findById": {
                    "get": {
                        "responses": {
                            "200": {
                                "description": "Specified pet",
                                "content" : {
                                    "application/json": {
                                        "schema": {
                                            "type": "object",
                                            "required": [
                                                "id",
                                                "name"
                                            ],
                                            "properties": {
                                                "id": {
                                                    "type": "integer",
                                                    "format": "int64"
                                                },
                                                "name": {
                                                    "type": "string"
                                                },
                                                "tag": {
                                                    "type": "string"
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                },
                "/pet/findByTags": {
                    "get": {
                        "responses": {
                            "200": {
                                "description": "Array of pets",
                                "content" : {
                                    "application/json": {
                                        "schema": {
                                            "type": "array",
                                            "items": {
                                                "type": "object",
                                                "required": [
                                                    "id",
                                                    "name"
                                                ],
                                                "properties": {
                                                    "id": {
                                                        "type": "integer",
                                                        "format": "int64"
                                                    },
                                                    "name": {
                                                        "type": "string"
                                                    },
                                                    "tag": {
                                                        "type": "string"
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        let pet = new ObjectSchema()
        .addProperty("id", new PrimitiveSchema("integer", "int64"), true)
        .addProperty("name", new PrimitiveSchema("string"), true)
        .addProperty("tag", new PrimitiveSchema("string"), false);

        let pets = new ArraySchema(pet);

        let findById = new Resource("findById");
        findById.addMethod("get")
        .setResponse(new Response(200)
            .addContent(new Content("application/json", pet))
            .setDescription("Specified pet")
        );

        let findByTags = new Resource("findByTags");
        findByTags.addMethod("get")
        .setResponse(new Response(200)
            .addContent(new Content("application/json", pets))
            .setDescription("Array of pets")
        );

        let api = new API("1.0.0", "Swagger Petstore", "MIT")
        .addResource(findById, "/pet/findById")
        .addResource(findByTags, "/pet/findByTags")
        .output();

        expect(JSON.parse(api)).to.be.deep.equal(actual);
    });
});

describe("Library", function() {

    it("should be able to produce the petstore API", function() {


        let pet = new Resource("pet");
        pet.addMethod("get").addResponse(
            new Response().setDescription("unexpected error")
        );


        let result = new API("1.0.0", "Swagger Petstore", "MIT")
        .addServer("http://petstore.swagger.io/v1")
        .addResource(pet)
        .output();

        //console.log("Result:", result); // todo, actual comparison

    });
});