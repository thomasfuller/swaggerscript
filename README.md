# SwaggerScript
SwaggerScript is a deeply embedded TypeScript language to make writing Swagger and OpenAPI specifications
easier and more friendly to humans.

# Links to Proposals

Link to Pre-Proposal (5.1)
https://docs.google.com/document/d/1TP85YXMyzaREDK_p9YDwhLyWUOt1p2eVIkzvsitCVPI/edit?usp=sharing

Link to Proposal (5.2)
https://docs.google.com/document/d/1eXYzXUQdSX0w31jWoWVqcHvMqsN54iKJe9VrXtushWA/edit?usp=sharing

Link to Design Document (5.3)
https://docs.google.com/document/d/1zmQqrbaRTe8_7mQRh7oJ2KE_hjstatyeK7OjjZ8H0vc/edit?usp=sharing

Link to Project Report (5.4)
https://docs.google.com/document/d/11VXNy3V6KylhB0WrsirWIBIIAvN4IMOTSSev7rnZh2o/edit?usp=sharing

Link to Project Slides (5.5)
https://drive.google.com/file/d/1pAoOWue15gSEBmn0uZpxuYEIjPF8NPAN/view?usp=sharing

Link to Video Demonstration
https://youtu.be/wJJmhsXZSog

# Tutorial

Getting started with SwaggerScript is simple.
First, you need to import the API object:
```
import * as S from "../src/S";
```
or
```
import { API, Resource, Parameter, PrimitiveSchema, ObjectSchema, ArraySchema, Response, Header, Content } from "../src/S";
```

To create an API, simply call the API constructor:
```
let api = new API("1.0.0", "Swagger Petstore", "MIT");
```

To add a resource to the API, call `addResource(Resource)` on the API.
```
let userPath = new Resource("users");
api.addResource(userPath);
```

To take advantage of reusability, we recommend defining shared schemas and
models before declaring shared methods. After models and methods are
declared, then declare and attach resources. For clarity, fluent syntax is
omitted here.
```
// Create the schema of a Pet object
let pet = new ObjectSchema()
.addProperty("id", new PrimitiveSchema("integer", "int64"), true)
.addProperty("name", new PrimitiveSchema("string"), true)
.addProperty("tag", new PrimitiveSchema("string"), false);

// Define pet as a content type
let petContent = new Content("application/json", new ArraySchema(pet));

// Create a 200 response to a request for a pet
let petOK = new Response(200);

// Attach the defined content type to the response
petOK.addContent(petContent));

// Create a method that will return a Pet object
let getPet = new Method("get");

// Attach the response to the method
getPet.addResponse(petOK);

// Create a resource and attach the method to it
let petPath = new Resource("pet");
petPath.addMethod(getPet);

// Attach the resource to an API
let api = new API();
api.addResource(petPath, "/pet");

// Print out the compiled API in a valid swagger string
console.log(api.output());
```

Each of the objects above can be reused. For example, if another method
will also return the pet content type, it could simply be reused.
Additionally, if there was another resource that needed the petPath
resource as a child, that could be completely reused by simply attaching it
to that resoruce in addition to the API itself.

For a complete example of using SwaggerScript, go to the examples
directory. petstore.ts is a SwaggerScript program that corresponds to the
petstore.json Swagger file. You can run that program with 
`tsc && node ./examples/petstore.js > petstore2.json`

petstore2.json is the file that contains the 
SwaggerScript-compiled version of the API.
